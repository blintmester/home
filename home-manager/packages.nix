{ config, pkgs, ... }:
let
  # add using `nix-channel --add https://nixos.org/channels/nixos-unstable nixos-unstable`
  unstable = import <nixos-unstable> { config = { allowUnfree = true; }; };
in
{
  home.packages = with pkgs; [
    nixos-option

    # basic utils
    tmux
    neofetch
    openssh
    dig
    nmap
    jq
    yq
    sshpass
    terraform
    pwgen
    file
    unzip
    yadm
    binwalk
    nix-index
    zip
    lsof
    unixtools.xxd
    # google-cloud-sdk
    # fancy curl with http3
    (curl.override { openssl = quictls; http3Support = true; })

    # shell bling-bling
    nerdfonts
    mcfly
    bat
    mmctl

    # dev land
    git
    git-lfs
    nodejs
    unstable.go
    unstable.golangci-lint
    unstable.kubectl
    unstable.kubernetes-helm
    unstable.ibmcloud-cli
    unstable.hugo

    # fun stuff
    cowsay
    fortune
    lolcat
    figlet
    cmatrix

    nix-top
  ];

  nixpkgs.config.allowUnfree = true;
}
