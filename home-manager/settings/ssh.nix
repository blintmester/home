{ ... }:
let
  deprecatedSecurity = {
    GSSAPIAuthentication = "yes";
    KexAlgorithms = "+diffie-hellman-group1-sha1,diffie-hellman-group-exchange-sha1,diffie-hellman-group14-sha1";
    Ciphers = "+3des-cbc,aes128-cbc,aes192-cbc,aes256-cbc,aes128-ctr,aes192-ctr,aes256-ctr";
    HostKeyAlgorithms = "+ssh-rsa";
  };
in
{
  programs.ssh = {
    enable = true;

    compression = true;
    controlMaster = "auto";
    controlPersist = "10m";
    controlPath = "~/.ssh/socket/%r@%h:%p";
    serverAliveInterval = 10;

    extraConfig = ''
      LogLevel DEBUG1
      ConnectTimeout 30
      IdentityFile ~/.ssh/keys/id_ed25519
    '';

    matchBlocks = {
      # HomeLab
      "hades" = {
#        hostname = "10.151.0.77";
        hostname = "192.168.42.254";
        user = "root";
        proxyJump = "hades-k3s";
        identityFile = "/home/blint/.ssh/keys/id_ed25519";
      };
      "newton" = {
        hostname = "10.151.0.33";
        user = "root";
        identityFile = "/home/blint/.ssh/keys/id_ed25519";
      };
      "rock5" = {
        hostname = "rock-5b.wombat-coho.ts.net";
      };
      "router" = {
        hostname = "blintrouter.wombat-coho.ts.net";
        user = "root";
      };
      "oogway" = {
        hostname = "oogway.wombat-coho.ts.net";
        user = "blint";
      };
      "hades-k3s" = {
        hostname = "152.66.211.131";
        user = "debian";
      };

      # KSZK
      "db-sch".hostname = "10.151.204.67";
      "noc-jump".hostname = "10.151.200.100";
      "noc-a" = {
        hostname = "noc-a.sch.bme.hu";
        port = 10022;
      };
      "noc-b" = {
        hostname = "noc-b.sch.bme.hu";
        port = 10022;
      };
      "sw-* rtr-1" = {
        proxyJump = "noc-jump";
        extraOptions = deprecatedSecurity;
      };
      "trash trash.sch.bme.hu" = {
        hostname = "trash.sch.bme.hu";
        port = 10022;
      };
      "beholder".hostname = "beholder.sch.bme.hu";
      "cctv".hostname = "cctv.sch.bme.hu";

      # IBM
      "nautilus" = {
        hostname = "localhost";
        port  = 10022;
        extraOptions = {
          StrictHostKeyChecking = "no";
        };
      };
    };
  };

  home.file.".ssh/socket/.keep".text = "";
}
