{ pkgs, ... }:
{
  programs.neovim = {
    enable = true;
    defaultEditor = true;
    viAlias = true;
    vimAlias = true;
    vimdiffAlias = true;
    extraLuaConfig = ''

      -- extra conf begin
      ${builtins.readFile ../files/nvim/options.lua}
      -- extra conf end

    '';
    plugins = with pkgs.vimPlugins; [
      {
        plugin = lsp-zero-nvim;
        type = "lua";
        config = "require('lsp-zero').setup { }";
      }

      {
        plugin = nvim-lspconfig;
        type = "lua";
        config = builtins.readFile ../files/nvim/lsp.lua;
      }

      {
        plugin = lazy-lsp-nvim;
        type = "lua";
        config = "require('lazy-lsp').setup { }";
      }

      luasnip
      friendly-snippets
      cmp_luasnip
      cmp-nvim-lsp
      {
        plugin = nvim-cmp;
        type = "lua";
        config = builtins.readFile ../files/nvim/completion.lua;
      }

      {
        plugin = comment-nvim;
        type = "lua";
        config = ''require("Comment").setup()'';
      }

      {
        plugin = tokyonight-nvim;
        config = "colorscheme tokyonight";
      }

      {
        plugin = neodev-nvim;
        type = "lua";
        config = "require('neodev').setup()";
      }

      {
        plugin = telescope-nvim;
        type = "lua";
        config = "require('telescope').setup()";
      }
      {
        plugin = telescope-fzf-native-nvim;
        type = "lua";
        config = "require('telescope').load_extension('fzf')";
      }

      {
        plugin = lualine-nvim;
        type = "lua";
        config = "require('lualine').setup({ icons_enabled = true, theme = 'tokyonight' })";
      }
      nvim-web-devicons

      {
        plugin = nvim-treesitter.withPlugins (p: with p; [
          tree-sitter-nix
          tree-sitter-vim
          tree-sitter-bash
          tree-sitter-lua
          tree-sitter-python
          tree-sitter-json
          tree-sitter-yaml
          tree-sitter-go
        ]);
        type = "lua";
        config = "require('nvim-treesitter.configs').setup {}";
      }

      vim-nix
    ];

    extraPackages = with pkgs; [
      wl-clipboard

      gopls
      golangci-lint-langserver

      marksman
    ];
  };
}