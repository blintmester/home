{ pkgs, lib, ... }:
let
  homeDir = builtins.getEnv "HOME";
  kubeFolder = builtins.toPath "${homeDir}/.kube";
  filesInKube = builtins.readDir kubeFolder;
  kubeYamls = builtins.filter (f: lib.hasSuffix ".yaml" "${f}") (builtins.attrNames filesInKube);
  kubeYamlsFullPath = map (f: "${kubeFolder}/${f}") kubeYamls;
  mainKubeConfig = "${kubeFolder}/config";
  kubeConfigs = [ mainKubeConfig ] ++ kubeYamlsFullPath;
  kubeconfig = builtins.concatStringsSep ":" kubeConfigs;
in
{
  programs.zsh = {
    enable = true;
    dotDir = ".config/zsh";
    autosuggestion.enable = true;
    syntaxHighlighting.enable = true;
    enableCompletion = true;
    enableVteIntegration = true;
    historySubstringSearch = {
      enable = true;
    };
    oh-my-zsh = {
      enable = true;
      plugins = [
        "git"
        #"adb"
        "colored-man-pages"
        "command-not-found"
        "golang"
        "ansible"
        "kubectl"
        "helm"
        "terraform"
      ];
      theme = "";
    };
    shellAliases = {
      cat = "bat -p";
      less = "less -r";
      ssh = "TERM=xterm-256color ssh";
      pkg = "NIXPKGS_ALLOW_UNFREE=1 nix-shell --run \"env SHELL='$(which zsh)' zsh\" -p";
      gcloud = "TERM=xterm-256color gcloud";
      watch = "viddy";
      c = "code --folder-uri $PWD";
      ic = "ibmcloud";
    };
    plugins = [
      {
        name = "powerlevel10k";
        src = pkgs.zsh-powerlevel10k;
        file = "share/zsh-powerlevel10k/powerlevel10k.zsh-theme";
      }
      {
        name = "powerlevel10k-config";
        src = lib.cleanSource ../files;
        file = "p10k.zsh";
      }
    ];
    initExtra = ''
      source /usr/local/ibmcloud/autocomplete/zsh_autocomplete
    '';
    envExtra = ''
      export PATH="$HOME/bin:$PATH:$HOME/go/bin"
      export PATH=/opt/homebrew/bin:/opt/homebrew/sbin:$PATH
    '';
  };

  programs.mcfly = {
    enable = true;
    enableZshIntegration = true;
    fuzzySearchFactor = 2;
  };

  home.sessionVariables = {
    KUBECONFIG = kubeconfig;
    MCFLY_RESULTS = "50";
  };
}
