{ config, pkgs, home, ... }:
{

  imports = [
      ./git.nix
      ./zsh.nix
      ./nvim.nix
      ./tmux.nix
      ./ssh.nix
  ];


  home.sessionVariables = {
    EDITOR = "nvim";
  };
}
