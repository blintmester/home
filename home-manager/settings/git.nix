{ config, pkgs, home, ... }:
{
  programs.git = {
    enable = true;
    userName = "Bálint Réthelyi (Blint)";
    userEmail = "me@blint.me";
    lfs.enable = true;
    signing = {
      signByDefault = true;
      key = "~/.ssh/keys/id_ed25519";
    };
    extraConfig = {
      gpg.format = "ssh";
      core = {
        autocrlf = "input";
        pager = "bat";
        editor = "code -w";
      };
      pull.rebase = "true";
      push.autoSetupRemote = "true";
      init.defaultBranch = "main";
      url = {
        "ssh://git@git.sch.bme.hu/" = {
          insteadOf = "https://git.sch.bme.hu/";
        };
#        "ssh://git@github.com/" = {
#          insteadOf = "https://github.com/";
#        };
        "ssh://git@github.ibm.com/" = {
          insteadOf = "https://github.ibm.com/";
        };
      };
    };
    includes = [
          {
            path = (pkgs.formats.ini {}).generate "ibm.gitconfig" {
              user.email = "balint.rethelyi@ibm.com";
            };
            condition = "gitdir:~/dev/ibm/**";
          }
        ];

  };
}
