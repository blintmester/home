{ config, pkgs, home, ... }:
let
    # choose between linux and mac
    spliced = builtins.split "-" builtins.currentSystem;
    hostOS = builtins.elemAt spliced 2;
    osImport = ./os + "/${hostOS}";
in
{
  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home.username = builtins.getEnv "USER";
  home.homeDirectory = builtins.getEnv "HOME";

  imports = [
    ./packages.nix
    ./settings
    osImport

  ];

  home.stateVersion = "24.05"; # Please read the comment before changing.

  home.sessionVariables = {
    EDITOR = "nvim";
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
