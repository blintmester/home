{ lib
, stdenv
, hyprland
, fetchFromGitHub
, gcc
, gnumake
, pkg-config
}:
stdenv.mkDerivation rec {
  pname = "hyprland-dwindle-autogroup";
  version = "0.1";
  src = fetchFromGitHub {
    owner = "ItsDrike";
    repo = "hyprland-dwindle-autogroup";
    rev = "bcb375d507d417b2695a0d6532990cc25f45bc31";
    sha256 = "sha256-8j1SjoVcQ7ccQMCKu8/ORThqtm9nBjtHurCWL9sSz7I=";
  };

  nativeBuildInputs = [ gnumake pkg-config ];

  buildInputs = [hyprland] ++ hyprland.buildInputs;

  homeStr = "\${HOME}";
  outStr = "\${out}";
  patchPhase = ''
    sed -i 's/$(MAKE) all//' Makefile
    sed -i 's/$(MAKE) clear//' Makefile
    sed -i 's/${homeStr}\/.local\/share\/hyprload\/plugins\/bin/${outStr}\/lib/' Makefile
    sed -i 's/PLUGIN_NAME=dwindle-autogroup/PLUGIN_NAME=libhyprland-dwindle-autogroup/' Makefile
  '';

  meta = with lib; {
    homepage = "https://github.com/ItsDrike/hyprland-dwindle-autogroup";
    description = "Sway-like group behavior";
    license = licenses.bsd3;
    platforms = platforms.linux;
  };
}