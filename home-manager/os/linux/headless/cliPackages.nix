{ pkgs, ... }:
{
  home.packages = with pkgs; [
    # tools
    usbutils
    woeusb-ng
    ventoy-full
    docker-credential-helpers
    sshfs
    progress
    grpcurl
    gnumake

    # everyday life
    pbpctrl
    unrar
    ddcutil

    jdk

    # AI meme
    ollama

    # work-y
    openshift
    docker
    yamllint
    ansible-lint
    
  ];
}
