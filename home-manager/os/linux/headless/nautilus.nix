{ pkgs, ... }:
{
  programs.go = {
    goPrivate = [ "github.ibm.com/*" ];
    goProxy = https://proxy.golang.org,direct;
    goNoProxy = [ "github.ibm.com" ];
    goSumDB = "off";
    go111Module = "on";
  };
}