{ pkgs, lib, ... }:
{
  wayland.windowManager.hyprland = let
    scope = "systemd-run --user --scope";
    swayosd = "${scope} ${pkgs.swayosd}/bin/swayosd";
  in {
    settings = {
      # bind, e=repeat, l=locked
      bindel = [
        ", XF86MonBrightnessUp, exec, ${swayosd} --brightness raise"
        ", XF86MonBrightnessDown, exec, ${swayosd} --brightness lower"
        ", XF86AudioRaiseVolume, exec, ${swayosd} --output-volume raise"
        ", XF86AudioLowerVolume, exec, ${swayosd} --output-volume lower"
        ", XF86AudioMute, exec, ${swayosd} --output-volume=mute-toggle"
      ];

      #layerrule = [ "blur, swayosd" ];
    };
  };

  services.swayosd = {
    enable = true;
  };
}
