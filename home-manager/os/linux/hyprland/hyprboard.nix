{ pkgs, ... }:
let
  hyprboardRepo = pkgs.fetchFromGitea {
    domain = "codeberg.org";
    owner = "miketth";
    repo = "hyprboard";
    rev = "v0.0.7";
    sha256 = "sha256-gZmB/cf0EE6aJJ/9Y27P9KZs3/HRV0rM7gJdFOMa2tE=";
  };
  hyprboard = pkgs.callPackage hyprboardRepo {};
in
{
  systemd.user.services.hyprboard = {
    Install.WantedBy = [ "graphical-session.target" ];
    Unit.Description = "Keyboard switch daemon";
    Service = {
      Type = "notify";
      ExecStart = "${hyprboard}/bin/hyprboard -evdev-xml-path ${pkgs.xkeyboard_config}/share/X11/xkb/rules/evdev.xml";
    };
  };
}
