{ pkgs, lib, ... }:
{
  programs.waybar = {
    enable = true;
    systemd = {
      enable = true;
      target = "graphical-session.target";
    };
    settings.mainBar = {
      layer = "top";
      position = "top";
      modules-left = [ "hyprland/workspaces" ];
      modules-right = [ "wireplumber" "network" "battery" "idle_inhibitor" "tray" "clock" ];

      idle_inhibitor = {
        format = "{icon} ";
        format-icons = {
          activated = "";
          deactivated = "";
        };
      };

      network = {
        format-ethernet = "";
        format-wifi = "   {signalStrength}%";
        format-disconnected = "";
        tooltip-format = "{essid} {ipaddr}/{cidr}";
        max-lenght = 10;
      };

      wireplumber = {
        format = "   {volume}%";
        format-muted = " ";
        on-click = lib.getExe pkgs.pavucontrol;
      };

      battery = {
        format = "   {capacity}%";
      };
    };
    style = ''
      * {
        border: none;
        border-radius: 0;
        font-size: 10pt;
        min-height: 0;
        color: white;
      }

      window#waybar {
        background: rgba(0,0,0,0.2);
        color: white;
        margin-bottom: .5em;
        /*border-radius: 0 0 1em 1em;*/
      }

      #workspaces, #wireplumber, #network, #battery, #idle_inhibitor, #tray, #clock {
        padding: 0 .5em;
        margin: 0 .5em;
      }
    '';
  };
  wayland.windowManager.hyprland.settings.layerrule = [ "blur, waybar" ];
}
