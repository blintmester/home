{ pkgs, ... }:
let
  dwindle-autogroup = pkgs.callPackage ./../packages/hyprland-dwindle-autogroup.nix { stdenv = pkgs.gcc13Stdenv; };
in
{
  wayland.windowManager.hyprland.plugins = [
    dwindle-autogroup
  ];
}