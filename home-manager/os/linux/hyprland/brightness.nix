{ pkgs, ... }:
let
  toml = pkgs.formats.toml { };
in
{
  systemd.user.services.wluma = {
    Install.WantedBy = [ "graphical-session.target" ];
    Unit.Description = "Brightness daemon";
    Service = {
      Restart = "on-failure";
      RestartSec = "5";
      ExecStart = "${pkgs.wluma}/bin/wluma";
    };
  };

  xdg.configFile."wluma/config.toml".text = ''
    [als.iio]
    path = "/sys/bus/iio/devices"
    thresholds = { 0 = "night", 20 = "dark", 80 = "dim", 250 = "normal", 500 = "bright", 800 = "outdoors" }

    [[output.backlight]]
    name = "eDP-1"
    path = "/sys/class/backlight/amdgpu_bl0"
    capturer = "wlroots"

  '';
}
