{ pkgs, lib, ... }:
let
  background-path = "${builtins.getEnv "HOME"}/Pictures/bg/bg.png";
  swaylock = lib.getExe pkgs.swaylock;
  lock = "${swaylock} -f -F -i ${background-path} --show-failed-attempts --daemonize --show-keyboard-layout --indicator-caps-lock"; # --color 000000";
  off = "${pkgs.hyprland}/bin/hyprctl dispatch dpms off";
  on = "${pkgs.hyprland}/bin/hyprctl dispatch dpms on";
  sleep = "${pkgs.systemd}/bin/systemctl suspend";

  lockAndDim = "/bin/sh -c '${lock} && sleep 5 && ${off}'";
in
{
  wayland.windowManager.hyprland.settings.bind = [
    "$mod, L, exec, ${lockAndDim}"
  ];

  services.swayidle = {
    enable = true;
    events = [
      { event = "before-sleep"; command = lock; }
      { event = "lock"; command = lock; }
      { event = "after-resume"; command = on; }
    ];
    timeouts = [
      { timeout = 300; command = lock; }
      { timeout = 315; command = off; resumeCommand = on; }
      { timeout = 600; command = sleep; }
    ];
  };
}
