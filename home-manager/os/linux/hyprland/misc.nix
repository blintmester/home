{ pkgs, lib, ... }:
{

  home.packages = with pkgs; [
    grim
    slurp
    networkmanagerapplet
    glib
    pavucontrol
  ];

  programs.kitty.settings.background_opacity = "0.5";

  services.network-manager-applet.enable = true;

  services.mako = {
    enable = true;
  };

  services.redshift = {
    enable = true;
    provider = "geoclue2";
  };

  systemd.user.services.polkit = {
    Install.WantedBy = [ "graphical-session.target" ];
    Unit.Description = "Polkit Agent";
    Service.ExecStart = "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
  };
}
