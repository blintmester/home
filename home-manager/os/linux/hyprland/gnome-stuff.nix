{ pkgs, ... }:
{
  home.packages = with pkgs.gnome; [
    baobab
    eog
    evince
    gnome-control-center
    gnome-system-monitor
    nautilus
    file-roller
    seahorse
    gnome-calculator

    pkgs.gnome-network-displays
    pkgs.gedit
  ];

  systemd.user.services = let
    gsdPath = "${pkgs.gnome-settings-daemon}/libexec";
    desiredServices = [
      "printer"
      "rfkill"
      "sharing"
      "sound"
      "wwan"
      "datetime"
      "power"
      "print-notifications"
      "smartcard"
      "usb-protection"
      "xsettings"
    ];
    gnomeServices = builtins.foldl' (acc: srv:
      let
        thisService = {
          Unit.Description = "Gnome Settings Daemon (${srv})";
          Install.WantedBy = [ "graphical-session.target" ];
          Service.ExecStart = "${gsdPath}/gsd-${srv}";
        };
      in acc // { "gsd-${srv}" = thisService; }
    ) {} desiredServices;
  in gnomeServices;
}
