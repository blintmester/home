{ pkgs, lib, ... }:
let
  background-path = "${builtins.getEnv "HOME"}/Pictures/bg/bg.png";
in
{
  home.packages = with pkgs; [
    swww
  ];

  systemd.user.services.swww = {
    Install.WantedBy = [ "graphical-session.target" ];
    Unit.Description = "Wallpaper Daemon";
    Service = {
      Type = "notify";
      ExecStart = "${pkgs.swww}/bin/swww-daemon";
      ExecStartPost = "${lib.getExe pkgs.swww} img ${background-path}";
      Restart = "on-failure";
    };
  };

  systemd.user.services.monitor-listener = let
    socat = lib.getExe pkgs.socat;
    swww = lib.getExe pkgs.swww;
    sleep = "${pkgs.coreutils}/bin/sleep";
    line = "\${line:0:12}";
    script = pkgs.writeText "monitor-listener.sh" ''
      #!/bin/sh

      set -eo pipefail

      echo Connecting to Hyprland $HYPRLAND_INSTANCE_SIGNATURE

      ${socat} -u UNIX-CONNECT:/tmp/hypr/$HYPRLAND_INSTANCE_SIGNATURE/.socket2.sock - | while read line; do
        if [[ "${line}" == "monitoradded" ]]; then
          echo Setting background
          ${sleep} 1s
          ${swww} img ${background-path}
        fi
      done

      echo Exiting
    '';
  in {
    Unit = {
      Description = "Apply wallpaper on new monitors";
      After = [ "swww.service" ];
    };
    Service = {
      ExecStart = "/bin/sh ${script}";
      Restart = "on-failure";
    };
    Install.WantedBy = [ "graphical-session.target" ];
  };
}
