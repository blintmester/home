{ ... }:
{
  imports = [
    ./hyprland.nix
    ./misc.nix
    ./osd.nix
    ./bar.nix
    ./wallpaper.nix
    ./lock-sleep.nix
    ./looks.nix
    ./plugins.nix
    ./gnome-stuff.nix
    ./hyprboard.nix
    ./brightness.nix
    ./rofi.nix
  ];
}
