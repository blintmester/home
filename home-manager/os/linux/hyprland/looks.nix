{ pkgs, ... }:
let
  iconsPkg = pkgs.adwaita-icon-theme;
  iconsName = "Adwaita";
in
{
  home.pointerCursor = {
    package = iconsPkg;
    name = iconsName;
    size = 16;
    gtk.enable = true;
  };

  gtk = {
    enable = true;
    iconTheme = {
      package = iconsPkg;
      name = iconsName;
    };
    theme = {
      package = pkgs.gnome-themes-extra;
      name = "Adwaita-dark";
    };
    gtk2.extraConfig = ''
      gtk-application-prefer-dark-theme=1
    '';
    gtk3.extraConfig = {
      gtk-application-prefer-dark-theme = 1;
    };
    gtk4.extraConfig = {
      gtk-application-prefer-dark-theme = 1;
    };
  };

  qt = {
    enable = true;
    platformTheme = "gnome";
    style = {
      name = "adwaita-dark";
      package = with pkgs; [ adwaita-qt adwaita-qt6 ];
    };
  };
}
