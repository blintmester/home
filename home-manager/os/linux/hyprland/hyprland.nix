{ pkgs, lib, ... }:
{

  home.packages = with pkgs; [
    grim
    slurp
    glib
    grimblast
  ];

  wayland.windowManager.hyprland = let
    scope = "systemd-run --user --scope";
    term = "${scope} kitty";
    workspaceNum = 10;
    workspaceSwitch = builtins.genList (n: "$mod, ${builtins.toString n}, workspace, ${builtins.toString n}") workspaceNum;
    workspaceMove = builtins.genList (n: "$mod SHIFT, ${builtins.toString n}, movetoworkspacesilent, ${builtins.toString n}") workspaceNum;

    grimblast = lib.getExe pkgs.grimblast;
    screenshot = "${scope} ${grimblast} --freeze --cursor copy area";
  in {
    enable = true;
    settings = {
      "$mod" = "SUPER";

      monitor = [
        "eDP-1, preferred, 0x0, 1.5"
        "desc:Ancor Communications Inc ASUS VX239 GCLMTJ022198, 1920x1080@60, -1920x0, 1"
        "desc:BNQ BenQ PJ 0x00000001, 1920x1080@60, 1504x0, 1"
        #"desc:Lenovo Group Limited E24-10 V905BERL, 1920x1080@60, 3424x0, 1"
        "desc:Samsung Electric Company LS27AG55x HNAT700378, 2560x1440@60, 1504x0, 1"
        #"desc:U.S. Robotics Inc 2CH AUDIO 0x00000001, 2560x1440@60, 3424x0, 1"
        "desc:Lenovo Group Limited E24-10 V905BF7C, 1920x1080@60, 5984x0, 1"
      ];
      xwayland.force_zero_scaling = true;
      env = [
        "XCURSOR_SIZE,24"
        "WLR_DRM_NO_MODIFIERS,1"
      ];

      input = {
        kb_layout = "gb,hu";
        kb_variant = ",102_qwertz_dot_nodead";
        kb_options = "grp:caps_toggle";
        numlock_by_default = true;

        follow_mouse = true;

        touchpad = {
          natural_scroll = true;
        };
      };
      general = {
        gaps_in = 5;
        gaps_out = 5;
        border_size = 2;
        "col.active_border" = "rgba(33ccffee) rgba(00ff99ee) 45deg";
        "col.inactive_border" = "rgba(595959aa)";

        layout = "dwindle";

        allow_tearing = false;
      };
      decoration = {
        rounding = 10;
        blur = {
          enabled = true;
          size = 3;
          passes = 3;
        };
      };
      animations = {
        enabled = true;
        bezier = "myBezier, 0.05, 0.9, 0.1, 1.05";
        animation = [
          "windows, 1, 7, myBezier"
          "windowsOut, 1, 7, default, popin 80%"
          "border, 1, 10, default"
          "borderangle, 1, 8, default"
          "fade, 1, 7, default"
          "workspaces, 1, 6, default"
         ];
      };

      dwindle = {
        preserve_split = true;
        no_gaps_when_only = true;
      };

      master = {
        new_is_master = true;
      };

      gestures = {
        workspace_swipe = true;
        workspace_swipe_forever = false;
        workspace_swipe_numbered = true;
        workspace_swipe_create_new = true;
      };

      bind = [
        "$mod, left, movefocus, l"
        "$mod, right, movefocus, r"
        "$mod, up, movefocus, u"
        "$mod, down, movefocus, d"
        "$mod SHIFT, left, movewindow, l"
        "$mod SHIFT, right, movewindow, r"
        "$mod SHIFT, up, movewindow, u"
        "$mod SHIFT, down, movewindow, d"
        "$mod CONTROL SHIFT, left, movecurrentworkspacetomonitor, l"
        "$mod CONTROL SHIFT, right, movecurrentworkspacetomonitor, r"
        "$mod CONTROL SHIFT, up, movecurrentworkspacetomonitor, u"
        "$mod CONTROL SHIFT, down, movecurrentworkspacetomonitor, d"

        "$mod, Q, killactive"
        "$mod SHIFT, M, exit"
        "$mod, SPACE, pin"
        "$mod SHIFT, SPACE, togglefloating"
        "ALT, TAB, changegroupactive"
        "$mod, W, togglegroup"
        "$mod SHIFT, W, moveoutofgroup"
        "$mod, E, togglesplit"
        "$mod, F, fullscreen"
        "$mod, RETURN, exec, ${term}"
        ", Print, exec, ${screenshot}"
      ] ++ workspaceMove ++ workspaceSwitch;

      # bind mouse
      bindm = [
        "$mod, mouse:272, movewindow" # LMB
        "$mod SHIFT, mouse:272, resizewindow" # LMB
        "$mod, mouse:273, resizewindow" # RMB
      ];

      misc = {
        disable_hyprland_logo = true;
        disable_splash_rendering = true;
        force_default_wallpaper = 0;
        vrr = 2;
        vfr = true;
        mouse_move_enables_dpms = true;
        key_press_enables_dpms = true;
        no_direct_scanout = false;
      };
    };
  };

  home.activation."rofiCacheDelete" = lib.hm.dag.entryAfter ["writeBoundary"] ''
    echo "Clearing Rofi drun cache..."
    $DRY_RUN_CMD rm -f $HOME/.cache/rofi-drun-desktop.cache
  '';
}
