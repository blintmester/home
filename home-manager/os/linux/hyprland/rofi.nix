{ pkgs, ... }:
let
  scope = "systemd-run --user --scope";
  rofi = "${scope} ${pkgs.rofi-wayland}/bin/rofi";
  menu = "${rofi} -show drun -i -show-icons -icon-theme Adwaita -drun-use-desktop-cache --allow-images";

  _rofimoji = "${scope} ${pkgs.rofimoji}/bin/rofimoji --skin-tone neutral --selector rofi --clipboarder wl-copy --typer wtype";
  type = "${_rofimoji} --action type";
  copy = "${_rofimoji} --action copy";
in
{
  programs.rofi = {
    enable = true;
    package = pkgs.rofi-wayland;
    theme = "Arc-Dark";
  };

  nixpkgs.overlays = [
    (final: prev: {
      rofi = prev.rofi-wayland;
    })
  ];

  wayland.windowManager.hyprland.settings.bind = [
    "$mod, D, exec, ${menu}"
    "$mod, ESCAPE, exec, ${type}"
    "$mod SHIFT, ESCAPE, exec, ${copy}"
  ];

  home.packages = with pkgs; [ wtype wl-clipboard rofimoji ];
}