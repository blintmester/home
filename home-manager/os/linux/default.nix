{ ... }:
{
  imports = [
    ./settings.nix
    ./headless
    ./desktop
  ];
}
