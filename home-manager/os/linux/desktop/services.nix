{ config, pkgs, home, lib, ... }:
let
  # add using `nix-channel --add https://nixos.org/channels/nixos-unstable nixos-unstable`
  unstable = import <nixos-unstable> { config = { allowUnfree = true; }; };
in
{
  services.mpris-proxy.enable = true;

  services.gnome-keyring = {
    enable = true;
    components = [ "ssh" "secrets" "pkcs11" ];
  };
  services.ssh-agent.enable = true;
#  systemd.user.sessionVariables = {
#     SSH_ASKPASS = lib.getExe pkgs.kdePackages.ksshaskpass;
#      SSH_ASKPASS_REQUIRE = "prefer";
#      SSH_AUTH_SOCK = "$XDG_RUNTIME_DIR/ssh-agent";
#    };

  systemd.user.sessionVariables = {
    SSH_AUTH_SOCK = "$XDG_RUNTIME_DIR/keyring/ssh";
  };
}
