# nix-channel --add https://github.com/nix-community/plasma-manager/archive/trunk.tar.gz plasma-manager
# nix-channel --update plasma-manager
{ lib, ... }:
let
  term = "kitty";
  wall = ../../../files/backgrounds/the_last_of_us_tree.jpeg;
  merge = lib.attrsets.recursiveUpdate;
in
{
 imports = [
     <plasma-manager/modules>
   ];


  programs.plasma = {
    enable = true;
    workspace = {
      clickItemTo = "select";
      lookAndFeel = "org.kde.breezedark.desktop";
      colorScheme = "Breeze Dark";
      iconTheme = "Breeze Dark";
      cursor.theme = "Breeze";
      wallpaper = wall;
      enableMiddleClickPaste = true;
      theme = "breeze-dark";
    };

    hotkeys.commands."terminal" = {
      name = "Launch terminal";
      key = "Meta+Return";
      command = term;
    };

    shortcuts = {
      kwin = {
        "Overview" = "Meta+S";
        "Make Window Fullscreen" = "Meta+F";
        "Show Desktop" = "Meta+D";
        "Walk Through Windows" = "Alt+Tab";
        "Window Close" = "Meta+W";
      };
      ksmserver = {
        "Lock Session" = "Meta+L";
        "Log Out" = "Ctrl+Alt+Del";
      };
    };

    fonts = rec {
      fixedWidth = {
        family = "FireCode Nerd Font";
        pointSize = 10;
      };
      general = {
        family = "Noto Sans";
        pointSize = 10;
      };
      menu = general;
      toolbar = general;
      windowTitle = general;
      small = {
        family = general.family;
        pointSize = 8;
      };
    };

    configFile = {
      "baloofilerc"."Basic Settings"."Indexing-Enabled" = false;
      "kwinrc"."Desktops"."Number" = {
        value = 1;
        # Forces kde to not change this value (even through the settings app).
#        immutable = true;
      };
    };

    input = {
      keyboard = {
        model = "pc105"; # ISO FTW
        layouts = [
          { layout = "gb"; }
          {
            layout = "hu";
            variant = "102_qwertz_dot_nodead";
          }
        ];
        options = [ "grp:caps_toggle" ];
        numlockOnStartup = "on";
        switchingPolicy = "winClass"; # "All windows of current application"
      };
      touchpads = [
        {
          name = "PIXA3854:00 093A:0274 Touchpad";
          vendorId = "093a";
          productId = "0274";
          enable = true;
          disableWhileTyping = true;
          tapToClick = true;
          tapAndDrag = true;
          naturalScroll = true;
          pointerSpeed = 0;
          rightClickMethod = "twoFingers";
          scrollMethod = "twoFingers";
          twoFingerTap = "rightClick";
        }
      ];
    };

    kscreenlocker = {
      timeout = 5; # Minutes
      autoLock = true;
      passwordRequired = true;
      passwordRequiredDelay = 10;
      lockOnResume = true;
      appearance = {
        alwaysShowClock = true;
        showMediaControls = true;
        wallpaper = wall;
      };
    };

    kwin = {
      nightLight = {
        enable = true;
        mode = "location";
        location = {
          latitude = "47.4611";
          longitude = "19.0168";
        };
      };
    };

    powerdevil = rec {
      AC = {
        powerProfile = "balanced";
        autoSuspend = {
          action = "sleep";
          idleTimeout = 15 * 60; # 15 Minutes
        };
        powerButtonAction = "sleep";
        dimDisplay = {
          enable = true;
          idleTimeout = 5 * 60;
        };
        inhibitLidActionWhenExternalMonitorConnected = true;
        turnOffDisplay = {
          idleTimeout = 10 * 60;
          idleTimeoutWhenLocked = 20;
        };
        whenSleepingEnter = "standby";
        whenLaptopLidClosed = "sleep";
      };
      battery = merge AC {
        autoSuspend.idleTimeout = 5 * 60; # 5 Minutes
        dimDisplay.idleTimeout = 2 * 60;
        turnOffDisplay.idleTimeout = 3 * 60;
      };
      lowBattery = merge battery {
        powerProfile = "powerSaving";
        dimDisplay.idleTimeout = 1 * 60;
        turnOffDisplay.idleTimeout = 2 * 60;
        displayBrightness = 30;
      };
    };
    session = {
      general.askForConfirmationOnLogout = true;
      sessionRestore.restoreOpenApplicationsOnLogin = "startWithEmptySession";
    };
  };
}
