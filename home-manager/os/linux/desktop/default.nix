{ ... }:
{
  imports = [
    ./guiPackages.nix
    ./services.nix
    ./settings.nix
    ./gnome.nix
#    ./kde.nix
  ];
}
