{ pkgs, ... }:
{
  dconf = {
      enable = true;
      settings."org/gnome/desktop/interface".color-scheme = "prefer-dark";
  };

  dconf.settings = {
      # make virt-manager use system libvirt
      "org/virt-manager/virt-manager/connections" = {
        autoconnect = ["qemu:///system"];
        uris = ["qemu:///system"];
      };
  };

  xdg = {
      enable = true;
      mime.enable = true;
  };
}