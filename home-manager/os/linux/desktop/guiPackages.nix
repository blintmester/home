{ pkgs, ... }:
let
  # add using `nix-channel --add https://nixos.org/channels/nixos-unstable nixos-unstable`
  unstable = import <nixos-unstable> { config = { allowUnfree = true; }; };
in
{
  home.packages = with pkgs; [
    # everyday things
    firefox
    google-chrome
    vlc
    obs-studio
    thunderbird
    kate
    jellyfin-media-player
    spotify
    gimp
    calibre
    ydotool
    evince
    unstable.smile

    # tools
    lens
    gnome-disk-utility
    qpwgraph
    filelight
    unstable.jetbrains-toolbox
    remmina
    clang
    inkscape

    # productivity
    kdenlive mediainfo glaxnimate
    davinci-resolve
    onlyoffice-bin
    libreoffice
    softmaker-office

    # messaging
    unstable.telegram-desktop
    slack
    unstable.element-desktop
    fractal

    # games
    unstable.prismlauncher

    # dev land
    unstable.vscode
    unstable.python3
  ];


  nixpkgs.config.firefox = {
    speechSynthesisSupport = true;
    nativeMessagingHosts.packages = [ pkgs.plasma-browser-integration ];
  };
}
