{ lib, ... }:
let
  term = "kitty";
  emojipicker = "smile";
  appswitchList = builtins.genList (n:
    { "switch-to-application-${builtins.toString (n+1)}" = []; }
  ) 9;
  appswitches = builtins.foldl' (acc: item: acc // item) {} appswitchList;

  workspaceMoveList = builtins.genList (n:
    { "move-to-workspace-${builtins.toString (n+1)}" = [ "<Super><Shift>${builtins.toString (n+1)}" ]; }
  ) 9;
  workspaceSwitchList = builtins.genList (n:
    { "switch-to-workspace-${builtins.toString (n+1)}" = [ "<Super>${builtins.toString (n+1)}" ]; }
  ) 9;
  workspaceBinds = builtins.foldl' (acc: item: acc // item) {} (workspaceMoveList ++ workspaceSwitchList);

  homeDir = builtins.getEnv "HOME";
  applicationsDir = builtins.toPath "${homeDir}/.local/share/applications";
  desktopFiles = builtins.readDir applicationsDir;
  fileNames = builtins.attrNames desktopFiles;
  idea = builtins.filter (f: lib.hasPrefix "jetbrains-idea-" f) fileNames;
  goland = builtins.filter (f: lib.hasPrefix "jetbrains-goland-" f) fileNames;
in
{
  dconf = {
    enable = true;
    settings = with lib.hm.gvariant; {
      "org/gnome/desktop/datetime".automatic-datetime = true;

      "org/gnome/desktop/input-sources" = {
        sources = map mkTuple [
          [ "xkb" "gb" ]
          [ "xkb" "hu+102_qwertz_dot_nodead"]
        ];
        xkb-options = [ "grp:caps_toggle" ];
        per-window = true;
      };

      "org/gnome/desktop/interface" = {
        color-scheme = "prefer-dark";
        clock-format = "24h";
        cursor-blink = false;
        cursor-size = 24;
        cursor-theme = "Adwaita";
        gtk-theme = "Adwaita-dark";
        icon-theme = "Adwaita";
        enable-animations = true;
        font-aliasing = "rgba";
        font-rgba-order = "rgb";
        font-hinting = "slight";
        font-name = "Cantarell 10";
        monospace-font-name = "FiraCode Nerd Font 10";
        gtk-enable-primary-paste = true;
        show-battery-percentage = true;
      };

      "org/gnome/desktop/media-handling" = {
        automount = false;
        automount-open = false;
      };

      "org/gnome/desktop/peripherals/touchpad" = {
        natural-scroll = true;
        tap-to-click = true;
        two-finger-scrolling-enabled = true;
      };

      "org/gnome/desktop/privacy" = {
        old-files-age = 30;
        usb-protection = true;
        usb-protection-level = "lockscreen";
      };

      "org/gnome/desktop/session".idle-delay = 300;

      "org/gnome/desktop/sound".allow-volume-above-100-percent = false;

      "org/gnome/desktop/wm/keybindings" = {
        close = [ "<Alt>F4" "<Super>w" ];
        switch-windows = [ "<Alt>Tab" ];
        switch-windows-backward = [ "<Shift><Alt>Tab" ];
        switch-applications = [ "<Super>Tab" ];
        switch-applications-backward = [ "<Shift><Super>Tab" ];
        minimize = [ "<Super>Down" "<Super>h" ];
        maximize = [ ];
        toggle-maximized = [ "<Super>Up" ];
        panel-run-dialog = [ ];
        switch-input-source = [ "XF86Keyboard" ];
        toggle-fullscreen = [ "<Super>F" ];
      } // workspaceBinds;

      "org/gnome/desktop/wm/preferences" = {
        action-double-click-titlebar = [ "toggle-maximize" ];
        action-middle-click-titlebar = [ "minimize" ];
        audible-bell = false;
        focus-mode = "sloppy";
        mouse-button-modifier = "<Super>";
        resize-with-right-button = true;
        theme = "Adwaita";
        num-workspaces = 9;
      };

      "org/gnome/mutter" = {
        dynamic-workspaces = true;
        workspaces-only-on-primary = true;
        edge-tiling = true;
        experimental-features = [ "scale-monitor-framebuffer" ];
      };

      "org/gnome/settings-daemon/plugins/media-keys".custom-keybindings = [
        "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/"
        "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/"
        "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/"
      ];
      "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0" = {
        binding = "<Control><Alt>t";
        command = term;
        name = "Open terminal";
      };
      "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1" = {
        binding = "<Super>Return";
        command = term;
        name = "Open terminal";
      };
      "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2" = {
              binding = "<Super>period";
              command = emojipicker;
              name = "Open emoji picker";
            };

      "org/gnome/settings-daemon/plugins/power" = {
        ambient-enabled = true;
        idle-dim = true;
        power-saver-profile-on-low-battery = true;
        sleep-inactive-ac-timeout = 900;
        sleep-inactive-ac-type = "suspend";
        sleep-inactive-battery-timeout = 600;
        sleep-inactive-battery-type = "suspend";
      };

      "org/gnome/settings-daemon/plugins/color" = {
        night-light-enabled = true;
        night-light-schedule-automatic = true;
        night-light-schedule-from = 21.0;
        night-light-schedule-to = 6.0;
        night-light-temperature = mkUint32 4200;
      };

      "org/gnome/shell/extensions/dash-to-dock" = {
        apply-custom-theme = true;
        background-opacity = 0.8;
        dash-max-icon-size = 48;
        dock-position = "BOTTOM";
        middle-click-action = "minimize";
        scroll-action = "cycle-windows";
        shift-click-action = "launch";
        shift-middle-click-action = "quit";
        multi-monitor = true;
        hot-keys = false;
        intellihide-mode = "ALL_WINDOWS";
        require-pressure-to-show = true;
        show-dock-urgent-notify = false;
      };

      "org/gnome/shell/extensions/caffeine".enable-fullscreen = true;

      "org/gnome/shell/extensions/blur-my-shell" = {
        color-and-noise = true;
        hacks-level = 3;
      };

      "org/gnome/shell/extensions/blur-my-shell/panel" = {
        override-background = true;
        static-blur = true;
      };

      # will add stuff here when fractional scaling works
      "org/gnome/shell/extensions/blur-my-shell/applications".whitelist = [];

      "org/gnome/shell/keybindings" = {
        show-screenshot-ui = "Print";
      } // appswitches;

      "org/gnome/shell/window-switcher" = {
        app-icon-mode = "both";
        current-workspace-only = false;
      };

      "org/gnome/shell" = {
        enabled-extensions = [
          "appindicatorsupport@rgcjonas.gmail.com"
          "drive-menu@gnome-shell-extensions.gcampax.github.com"
          "caffeine@patapon.info"
          "gsconnect@andyholmes.github.io"
        ];
        favorite-apps = [
#          "firefox.desktop"
          "google-chrome.desktop"
          "org.gnome.Nautilus.desktop"
#          "org.gnome.Calendar.desktop"
        ] ++ idea ++ goland ++ [
          "kitty.desktop"
        ];
        remember-mount-password = true;
      };

      "org/gnome/system/location".enabled = true;

      "org/gtk/settings/file-chooser" = {
        sort-column = "modified";
        sort-directories-first = true;
        sort-order = "descending";
        startup-mode = "recent";
      };

      "org/gtk/gtk4/settings/file-chooser" = {
        sort-column = "modified";
        sort-directories-first = true;
        sort-order = "descending";
      };
    };
  };
}