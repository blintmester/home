{ pkgs, ... }:
{
  systemd.user.sessionVariables = {
    EDITOR = "nvim";
    VISUAL = "nvim";
    JAVA_HOME = "${pkgs.openjdk}/lib/openjdk";
  };
}