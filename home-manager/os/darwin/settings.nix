{ pkgs, home, ... }:
{
  home.sessionVariablesExtra = ''
    export SSH_AUTH_SOCK="$HOME/Library/Group Containers/2BUA8C4S2C.com.1password/t/agent.sock"
  '';

  programs.go = {
      goPrivate = [ "github.ibm.com/*" ];
  };

}