{ config, pkgs, home, ... }:
{
  imports = [
    ./packages.nix
    ./settings.nix
  ];
}
