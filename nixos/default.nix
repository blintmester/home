{ lib, ... }:
let
  hostNameRaw = builtins.readFile /etc/hostname;
  hostName = lib.strings.removeSuffix "\n" hostNameRaw;
  hostImport = ./hosts + "/${hostName}.nix";
in
{
  imports = [
    ./gui.nix
    ./hardware.nix
    ./locale.nix
    ./magic.nix
    ./me.nix
    ./misc-config.nix
    ./networking.nix
    ./packages.nix
    ./services.nix
    ./system.nix
    ./audio-tricks.nix
#    ./auth.nix
    hostImport
  ];
  
  system.stateVersion = "24.11";
}
