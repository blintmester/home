{ pkgs, ... }:
{
    environment.sessionVariables = {
        # for chromium based apps
        NIXOS_OZONE_WL = "1";
    };

     hardware.graphics = {
       enable = true;
       extraPackages = with pkgs; [
         rocmPackages.clr.icd
       ];
     };
}