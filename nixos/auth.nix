{ pkgs, lib, ... }:
{
  # no fprintd by default to not mess with pw stuff
  security.pam.services.login.fprintAuth = false;



  # use fprintd for gdm's fingerprint path
  # also enable gnome-keyring unlocking with LUKS password
  security.pam.services.gdm-fingerprint.text = ''
    auth       required                    ${pkgs.fprintd}/lib/security/pam_fprintd.so
    auth       optional                    ${pkgs.gdm}/lib/security/pam_gdm.so
    auth       optional                    ${pkgs.gnome-keyring}/lib/security/pam_gnome_keyring.so
    account    include                     login
    password   required                    pam_deny.so
    session    include                     login
    session    optional                    ${pkgs.gnome-keyring}/lib/security/pam_gnome_keyring.so auto_start
  '';

  # enable gnome-keyring unlocking with LUKS password
  security.pam.services.gdm-autologin.text = ''
    auth       [success=ok default=1]      ${pkgs.gdm}/lib/security/pam_gdm.so
    auth       optional                    ${pkgs.gnome-keyring}/lib/security/pam_gnome_keyring.so
    auth       substack                    login
    account    include                     login
    password   required                    pam_deny.so
    session    include                     login
    session    optional                    ${pkgs.gnome-keyring}/lib/security/pam_gnome_keyring.so auto_start
  '';

  # enable gnome-keyring unlocking
  security.pam.services.gdm-password.text = lib.mkForce ''
    auth       substack                    login
    auth       optional                    ${pkgs.gnome-keyring}/lib/security/pam_gnome_keyring.so
    account    include                     login
    password   substack                    login
    session    include                     login
    session    optional                    ${pkgs.gnome-keyring}/lib/security/pam_gnome_keyring.so auto_start
  '';
}
