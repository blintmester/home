{ config, pkgs, ... }:
{
  boot.loader.systemd-boot = {
    enable = true;
    configurationLimit = 10;
  };
  boot.loader.efi.canTouchEfiVariables = true;
  boot.initrd.systemd.enable = true;
  boot.plymouth.enable = true;
  boot.kernelParams = [ "quiet" ];

  systemd = {
      extraConfig = "DefaultTimeoutStopSec=10s";
      user.extraConfig = "DefaultTimeoutStopSec=10s";
    };

}
