{ pkgs, ...}:
#let
  # add using `nix-channel --add https://nixos.org/channels/nixos-unstable nixos-unstable`
  #unstable = import <nixos-unstable> {
#    config = {
#        allowUnfree = true;
#        permittedInsecurePackages = [ "electron-24.8.6" ];
#    };
#in
{
    nix = {
        gc = {
          automatic = true;
          dates = "weekly";
          options = "--delete-older-than 14d";
        };
    };

    environment.systemPackages = with pkgs; [
        neovim
        rsync
        htop
        pavucontrol
        plasma-pa
        htop
        steam
        plasma5Packages.plasma-thunderbolt
        zsh
        kitty
        wl-clipboard
        virt-manager
        nixos-option
        sysstat
        openiscsi
        lm_sensors
        tpm2-tss
        jmtpfs
        libmtp
    ];

    programs.neovim = {
        enable = true;
        defaultEditor = true;
        viAlias = true;
        vimAlias = true;
        withNodeJs = true;
        withPython3 = true;
    };

    programs.adb.enable = true;

    programs.steam = {
        enable = true;
        remotePlay.openFirewall = true;
    };

    programs.gamescope = {
        enable = true;
        #capSysNice = true;
      };

    programs.mtr.enable = true;
    programs.wireshark = {
        enable = true;
        package = pkgs.wireshark-qt;
    };

    programs.dconf.enable = true;

    programs.zsh.enable = true;
    users.defaultUserShell = pkgs.zsh;
    environment.shells = with pkgs; [ zsh ];

    fonts.packages = with pkgs; [
        noto-fonts
        noto-fonts-extra
        noto-fonts-emoji
        fira-code
        fira-code-symbols
        nerdfonts
    ];

    nixpkgs.config = {
        allowUnfree = true;
        permittedInsecurePackages = [
          "electron-24.8.6"
          "fluffychat-linux-1.23.0"
        ];
    };
}
