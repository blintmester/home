{ pkgs, lib, config, ... }:
let
  outputName = "alsa_output.pci-0000_c1_00.6.analog-stereo";
  prettyName = "Framework Speakers";

  # 10 ** (db / 20), but Nix doesn't have pow()
  db = {
    "-18.1" = 0.1244514611771385;
    "-5.48" = 0.5321082592667942;
    "-4.76" = 0.5780960474057181;
    "8.1" = 2.5409727055493048;
    "-36" = 0.015848931924611134;
  };

  json = pkgs.formats.json {};
  filter-chain = json.generate "filter-chain.json" {
    "node.description" = prettyName;
    "media.name" = prettyName;
    "filter.graph" = {
      nodes = [
        {
          "type" = "lv2";
          "plugin" = "https://chadmed.au/bankstown";
          "name" = "bassex";
          "control" = {
            "bypass" = 0;
            "amt" = 1.2;
            "sat_second" = 1.3;
            "sat_third" = 2.5;
            "blend" = 1.0;
            "ceil" = 200.0;
            "floor" = 20.0;
          };
        }
        {
          "type" = "lv2";
          "plugin" = "http://lsp-plug.in/plugins/lv2/loud_comp_stereo";
          "name" = "el";
          "control" = {
            "enabled" = 1;
            "input" = 1.0;
            "fft" = 4;
          };
        }
        {
          type = "lv2";
          plugin = "http://lsp-plug.in/plugins/lv2/para_equalizer_x8_lr";
          name = "lappy";
          control = {
            mode = 0;
            react = 0.20000;
            zoom = db."-36";

            fl_0 = 101.00000;
            fml_0 = 0;
            ftl_0 = 5;
            gl_0 = db."-18.1";
            huel_0 = 0.00000;
            ql_0 = 4.36000;
            sl_0 = 0;
            wl_0 = 4.00000;

            fl_1 = 451.00000;
            fml_1 = 0;
            ftl_1 = 1;
            gl_1 = db."-5.48";
            huel_1 = 0.03125;
            ql_1 = 2.46000;
            sl_1 = 0;
            wl_1 = 4.00000;

            fl_2 = 918.00000;
            fml_2 = 0;
            ftl_2 = 1;
            gl_2 = db."-4.76";
            huel_2 = 0.06250;
            ql_2 = 2.44000;
            sl_2 = 0;
            wl_2 = 4.00000;

            fl_3 = 9700.00000;
            fml_3 = 0;
            ftl_3 = 1;
            gl_3 = db."8.1";
            huel_3 = 0.09375;
            ql_3 = 2.00000;
            sl_3 = 0;
            wl_3 = 4.00000;

            fr_0 = 101.00000;
            fmr_0 = 0;
            ftr_0 = 5;
            gr_0 = db."-18.1";
            huer_0 = 0.00000;
            qr_0 = 4.36000;
            sr_0 = 0;
            wr_0 = 4.00000;

            fr_1 = 451.00000;
            fmr_1 = 0;
            ftr_1 = 1;
            gr_1 = db."-5.48";
            huer_1 = 0.03125;
            qr_1 = 2.46000;
            sr_1 = 0;
            wr_1 = 4.00000;

            fr_2 = 918.00000;
            fmr_2 = 0;
            ftr_2 = 1;
            gr_2 = db."-4.76";
            huer_2 = 0.06250;
            qr_2 = 2.44000;
            sr_2 = 0;
            wr_2 = 4.00000;

            fr_3 = 9700.00000;
            fmr_3 = 0;
            ftr_3 = 1;
            gr_3 = db."8.1";
            huer_3 = 0.09375;
            qr_3 = 2.00000;
            sr_3 = 0;
            wr_3 = 4.00000;
          };
        }
        {
          "type" = "lv2";
          "plugin" = "http://lsp-plug.in/plugins/lv2/mb_compressor_stereo";
          "name" = "woofer_bp";
          "control" = {
            "mode" = 0;
            #"cbe_0" = 1;
            "ce_0" = 1;
            "sla_0" = 5.0;
            "cr_0" = 1.75;
            "al_0" = 0.725;
            "at_0" = 1.0;
            "rt_0" = 100;
            "kn_0" = 0.125;
            "cbe_1" = 1;
            "sf_1" = 200.0;
            "ce_1" = 0;
            "cbe_2" = 0;
            "ce_2" = 0;
            "cbe_3" = 0;
            "ce_3" = 0;
            "cbe_4" = 0;
            "ce_4" = 0;
            "cbe_5" = 0;
            "ce_5" = 0;
            "cbe_6" = 0;
            "ce_6" = 0;
          };
        }
        {
          "type" = "lv2";
          "plugin" = "http://lsp-plug.in/plugins/lv2/compressor_stereo";
          "name" = "woofer_lim";
          "control" = {
            "sla" = 5.0;
            "al" = 1.0;
            "at" = 1.0;
            "rt" = 100.0;
            "cr" = 15.0;
            "kn" = 0.5;
          };
        }
      ];
      "links" =  [
        {"output" =  "bassex:out_l"; "input" =  "el:in_l";}
        {"output" =  "bassex:out_r"; "input" =  "el:in_r";}

        {"output" =  "el:out_l"; "input" =  "lappy:in_l";}
        {"output" =  "el:out_r"; "input" =  "lappy:in_r";}

        {"output" = "lappy:out_l"; "input" = "woofer_bp:in_l";}
        {"output" = "lappy:out_r"; "input" = "woofer_bp:in_r";}

        {"output" =  "woofer_bp:out_l"; "input" =  "woofer_lim:in_l";}
        {"output" =  "woofer_bp:out_r"; "input" =  "woofer_lim:in_r";}
      ];
      "inputs" =  [
        "bassex:in_l"
        "bassex:in_r"
      ];
      "outputs" =  [
        "woofer_lim:out_l"
        "woofer_lim:out_r"
      ];
      "capture.volumes" =  [
        {
          "control" =  "el:volume";
          "min" =  -47.5;
          "max" =  0.0;
          "scale" =  "cubic";
        }
      ];
    };
    "capture.props" = {
      "node.name" = "audio_effect.laptop-convolver";
      "media.class" = "Audio/Sink";
      "audio.channels" = "2";
      "audio.position" = [ "FL" "FR" ];
      "device.api" = "dsp";
      "node.virtual" = "false";

      # Lower seems to mean "more preferred",
      # bluetooth devices seem to be ~1000, speakers seem to be ~2000
      # since this is between the two, bluetooth devices take over when they connect,
      # and hand over to this instead of the speakers when they disconnect.
      "priority.session" = 1666;
      "priority.driver" = 1666;
      "state.default-volume" = 0.343;
      "device.icon-name" = "audio-card-analog-pci";
    };
    "playback.props" = {
      "node.name" = "audio_effect.laptop-convolver";
      "target.object" = outputName;
      "node.passive" = "true";
      "audio.channels" = "2";
      "audio.position" = [ "FL" "FR" ];
      "device.icon-name" = "audio-card-analog-pci";
    };
  };

  lv2Plugins = with pkgs; [ lsp-plugins bankstown-lv2 ];
  lv2SearchPath = lib.makeSearchPath "lib/lv2" lv2Plugins;

  configPackage = (pkgs.writeTextDir "share/wireplumber/wireplumber.conf.d/99-laptop.conf" ''
    node.software-dsp.rules = [
      {
        matches = [{ node.name = "${outputName}" }]
        actions = {
          create-filter = {
            filter-path = "${filter-chain}"
            hide-parent = true
          }
        }
      }
    ]

    wireplumber.profiles = {
      main = { node.software-dsp = "required" }
    }
  '') // { passthru.requiredLv2Packages = with pkgs; [ lsp-plugins bankstown-lv2 ]; };
  #wpSearchPath = lib.makeSearchPath "share" [ configPackage config.services.pipewire.wireplumber.package ];
in
{
  services.pipewire.wireplumber.configPackages = [ configPackage ];
  services.pipewire.extraConfig.pipewire."69-rates" = {
    "context.properties" = {
      "default.clock.allowed-rates" = [ 44100 48000 88200 96000 176400 192000 ];
      "default.clock.rate" = 192000;
    };
  };
  services.pipewire.wireplumber.extraConfig."69-bluetooth-rates" = {
    "monitor.bluez.properties" = {
      "bluez5.default.rate" = 96000;
    };
  };
}
