{ config, pkgs, ... }:
{
    # Define a user account. Don't forget to set a password with ‘passwd’.
    users.users.blint = {
      isNormalUser = true;
      description = "Bálint Réthelyi";
      extraGroups = [ "networkmanager" "wheel" "libvirtd" "input" "wireshark" "adbusers" "rtkit" "dialout" "video" ];
      shell = pkgs.zsh;
    };
}
