{ config, pkgs, ... }:
let
  # add using `nix-channel --add https://nixos.org/channels/nixos-unstable nixos-unstable`
  unstable = import <nixos-unstable> { config = { allowUnfree = true; }; };
in
{
  # install using `nix-channel --add https://github.com/NixOS/nixos-hardware/archive/master.tar.gz nixos-hardware`
  imports = [ <nixos-hardware/framework/13-inch/7040-amd> ];
  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.kernelParams = [
    "amdgpu.dcdebugmask=0x10"
  ];
#  hardware.amdgpu.opencl = true;
  environment.sessionVariables = {
    RADV_PERFTEST = "rt";
    VKD3D_CONFIG = "dxr,dxr11";
  };

  hardware.framework.amd-7040.preventWakeOnAC = true;
  environment.systemPackages = [ pkgs.amdgpu_top ];

  nixpkgs.config.rocmSupport = true;

  nixpkgs.overlays = [
    (final: prev: {
      power-profiles-daemon = unstable.power-profiles-daemon;
    })
  ];

  # needed for cros_ec in Linux < 6.10
  # boot.kernelPatches = [
  #  {
  #    name = "fw-1";
  #    patch = pkgs.fetchurl {
  #      name = "fw-1.patch";
  #      url = "https://lore.kernel.org/chrome-platform/20231005160701.19987-3-dustin@howett.net/raw";
  #      hash = "sha256-0GblPfPhbTT0/bzz2/QPbRinP27zkD9sI1WqWjXtC24=";
  #    };
  #  }
  #  {
  #    name = "fw-2";
  #    patch = pkgs.fetchurl {
  #      name = "fw-2.patch";
  #      url = "https://lore.kernel.org/chrome-platform/20231005160701.19987-4-dustin@howett.net/raw";
  #      hash = "sha256-l0p4hqE6Hy3BIrBpdLkHkRNPByUOjrQ2L3FOYQf/ca8=";
  #    };
  #  }
  #  {
  #    name = "fw-3";
  #    patch = pkgs.fetchurl {
  #      name = "fw-3.patch";
  #      url = "https://lore.kernel.org/chrome-platform/20231005160701.19987-5-dustin@howett.net/raw";
  #      hash = "sha256-a5BV9t6IPOPOVJ8LxW+8J2roIDoCYrohitC9kljT6lw=";
  #    };
  #  }
  #  {
  #    name = "fw-4";
  #    patch = pkgs.fetchurl {
  #      name = "fw-4.patch";
  #      url = "https://lore.kernel.org/chrome-platform/20231005160701.19987-6-dustin@howett.net/raw";
  #      hash = "sha256-LARAxF2AZRcL+lkFe8D56RJw3IKWBAbTm0olkP7lVns=";
  #    };
  #  }
  #];
}
