{ config, pkgs, ... }:
#let
  # add using `nix-channel --add https://nixos.org/channels/nixos-unstable nixos-unstable`
  #unstable = import <nixos-unstable> { config = { allowUnfree = true; }; };
#in

{
  networking.networkmanager = {
    enable = true;
    enableStrongSwan = true;
  };
  networking.nftables.enable = true;

  services.tailscale = {
      enable = true;
      useRoutingFeatures = "client";
    };


  services.resolved = {
    enable = true;
    fallbackDns = [ "1.1.1.1#one.one.one.one" "1.0.0.1#one.one.one.one" ];
  };

  networking.firewall = {
    enable = true;
    allowedTCPPorts = [ 8080 8081 8000 ];
  };

  networking.interfaces.nonexistant0 = {
      virtual = true;
      ipv4.addresses = [{ address = "198.51.100.0"; prefixLength = 32; }];
  };

  systemd.services.NetworkManager-wait-online.enable = false;
}
