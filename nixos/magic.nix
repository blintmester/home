{ config, pkgs, lib, ... }:
{
  systemd.services.ydotool = {
    enable = true;
    description = "ydotool";
    serviceConfig = {
      Type = "simple";
      ExecStart = "${pkgs.ydotool}/bin/ydotoold --socket-own=1000:0";
    };
    wantedBy = [ "multi-user.target" ];
  };
  environment.sessionVariables."YDOTOOL_SOCKET" = "/tmp/.ydotool_socket";

  # Make /bin/bash available for non-Nix-aware programs
  system.activationScripts.binbash = "rm -f /bin/bash; ln -sf '${pkgs.bash}/bin/bash' /bin/bash";

  # Make non-Nix-aware programs work with Nix
  programs.nix-ld = {
      enable = true;
      libraries =
        (pkgs.appimageTools.defaultFhsEnvArgs.targetPkgs pkgs)
        ++ (pkgs.appimageTools.defaultFhsEnvArgs.multiPkgs pkgs);
    };

  # nix-ld places env vars in /etc/profile, let's fix that
  environment.sessionVariables."NIX_LD" = "/run/current-system/sw/share/nix-ld/lib/ld.so";
  environment.sessionVariables."NIX_LD_LIBRARY_PATH" = "/run/current-system/sw/share/nix-ld/lib";

  # Magic FS that follows symlinks to make flatpak host icons/fonts work
  system.fsPackages = [ pkgs.bindfs ];
  fileSystems = let
    mkRoSymBind = path: {
      device = path;
      fsType = "fuse.bindfs";
      options = [ "ro" "resolve-symlinks" "x-gvfs-hide" ];
    };
    aggregatedFonts = pkgs.buildEnv {
      name = "system-fonts";
      paths = config.fonts.packages;
      pathsToLink = [ "/share/fonts" ];
    };
  in {
    # Create an FHS mount to support flatpak host icons/fonts
    "/usr/share/icons" = mkRoSymBind (config.system.path + "/share/icons");
    "/usr/share/fonts" = mkRoSymBind (aggregatedFonts + "/share/fonts");
  };
}
