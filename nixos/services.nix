{ config, pkgs, ... }:
#let
#  # add using `nix-channel --add https://nixos.org/channels/nixos-unstable nixos-unstable`
#  unstable = import <nixos-unstable> { config = { allowUnfree = true; }; };
#in
{
  services.openssh.enable = true;
  services.hardware.bolt.enable = true;
  services.flatpak.enable = true;
  services.gnome.gnome-keyring.enable = true;

  system.fsPackages = [ pkgs.fuse-overlayfs ];
  virtualisation = {
    podman = {
      enable = true;
      defaultNetwork.settings = {
        dns_enabled = true;
      };
    };
    docker.rootless = {
      enable = true;
      setSocketVariable = true;
      daemon.settings = {
        dns = ["198.51.100.0"];
        ipv6 = true;
        fixed-cidr-v6 = "fd00::/80";
      };
    };
    waydroid.enable = true;
    libvirtd.enable = true;
  };
}
