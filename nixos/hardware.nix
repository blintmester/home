{ config, pkgs, ... }:
{
  services.fwupd.enable = true;
  hardware.bluetooth = {
    enable = true;
    settings = {
      General = {
        Experimental = true;
      };
    };
  };
  hardware.enableRedistributableFirmware = true;

  services.printing = {
    enable = true;
    drivers = [ pkgs.brlaser ];
  };

  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
    wireplumber.enable = true;
  };

  environment.systemPackages = with pkgs; [
    tpm2-tools
    vulkan-validation-layers
  ];

  # enable sysrq
  boot.kernel.sysctl = {
    "kernel.sysrq" = 1;
  };

  security.tpm2.enable = true;
  security.tpm2.pkcs11.enable = true;
  security.tpm2.tctiEnvironment.enable = true;
  users.users.blint.extraGroups = [ "tss" ];
}
