{ ... }:
{
  imports = [
    ./system.nix
    ./home-manager.nix
    ./me.nix
    ./settings.nix
    ./homebrew.nix
  ];
}
