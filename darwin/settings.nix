{ inputs, pkgs, ... }:
{
  # Replace aliases with app trampolines to make Spotlight work
#  imports = [ inputs.mac-app-util.darwinModules.default ];

  fonts.packages = with pkgs; [
    noto-fonts
    noto-fonts-cjk-sans
    font-awesome
    fira-code-nerdfont
    source-han-sans
    source-han-sans-japanese
    source-han-serif-japanese
  ];
}
