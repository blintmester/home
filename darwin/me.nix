{ config, pkgs, ... }:
{
    # Define a user account. Don't forget to set a password with ‘passwd’.
    users.users.blint = {
      description = "Bálint Réthelyi";
      home = "/Users/blint";
    };

    # staff is Darwin's wheel
     nix.settings.trusted-users = [ "@staff" ];
}
