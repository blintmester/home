{ ... }:
{
  homebrew = {
    enable = true;
    brews = [
      "lima"  # using nix package instead
    ];
    casks = [
      "unnaturalscrollwheels"
      "middleclick"
      "lens"
      "tunnelblick"
      "inkscape"
      "gimp"
      # "karabiner-elements"  # last working version is 14.13.0
      # downloaded from https://github.com/pqrs-org/Karabiner-Elements/releases/download/v14.13.0/Karabiner-Elements-14.13.0.dmg
    ];
  };
}
