{ config, pkgs, ... }:
{
    imports = [ <home-manager/nix-darwin> ];

    home-manager = {
        extraSpecialArgs = {
          osImport = ./../home-manager/os/darwin;
          userName = "blint";
          homeDir = "/Users/blint";
        };
        users.blint = import ./../home-manager;
      };

}