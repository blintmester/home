{ pkgs, inputs, ... }:
{
#  imports = [ inputs.nh_darwin.nixDarwinModules.prebuiltin ];

  environment.systemPackages = with pkgs; [
    htop
    procps
    darwin.iproute2mac
    kitty
    # lima
  ];

  security.pam.enableSudoTouchIdAuth = true;

  # Auto upgrade nix package and the daemon service.
  services.nix-daemon.enable = true;
  nix = {
    package = pkgs.nix;
    settings = {
      experimental-features = [
        "nix-command"
        "flakes"
      ];
    };
    # Auto-optimise is broken on Darwin: https://github.com/NixOS/nix/issues/7273
    optimise = {
      automatic = true;
      # Saturday 00:00
      interval = [{
        Weekday = 7;
        Hour = 0;
        Minute = 0;
      }];
      user = "blint";
    };
  };

#  programs.nh = {
#    enable = true;
#    clean = {
#      enable = true;
#      extraArgs = "--keep-since 7d --keep 3";
#    };
#  };

  services.tailscale = {
    enable = true;
    #useRoutingFeatures = "client";
  };

    # Create /etc/zshrc that loads the nix-darwin environment.
  programs.zsh.enable = true;

  system.stateVersion = 5;
}
