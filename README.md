## Home-manager

create some neccessary directories and files:

```bash
mkdir ~/.kube
touch ~/.zsh_history
```

create a file `home.nix` in your `.config/home-manager` directory and add the following content:

```nix
{ ... }:
{
  imports = [
    /home/user/dev/nix-dots/home-manager
  ];
}
```

### with only nixpkgs

```bash
nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager
nix-channel --add https://nixos.org/channels/nixos-unstable nixos-unstable
nix-channel --update
nix-shell '<home-manager>' -A install
```

## NixOS 

in the file `configuration.nix` in your `/etc/nixos` directory append the following content in the imports section:

```nix
# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:
{
  imports =
    [ # Include the results of the hardware scan.
      <nixos-hardware/framework/13-inch/7040-amd>
      ./hardware-configuration.nix
      #/home/blint/.config/nixos
      /home/blint/dev/nix-dots/nixos
    ];

  boot.initrd.luks.devices."luks-XXXX-XXXX-XXXX-XXXX-XXXX".device = "/dev/disk/by-uuid/XXXX-XXXX-XXXX-XXXX-XXXX";
  boot.initrd.luks.devices."luks-XXXX-XXXX-XXXX-XXXX-XXXX".allowDiscards = true;
  fileSystems."/".options = [ "noatime" "nodiratime" "discard" ];

  networking.hostName = "pervinca"; # Define your hostname.
  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

}
```

## nix-darwin

Use determinate nix installer:
```bash
curl --proto '=https' --tlsv1.2 -sSf -L https://install.determinate.systems/nix | \
  sh -s -- install
```

Then install nix-darwin  module:
```bash
nix-build https://github.com/LnL7/nix-darwin/archive/master.tar.gz -A installer
```

Add nix-channels:
```bash
nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager # or if you follow a Nixpkgs version 24.05 channel you can run
# nix-channel --add https://github.com/nix-community/home-manager/archive/release-24.05.tar.gz home-manager
nix-channel --add https://nixos.org/channels/nixos-unstable nixos-unstable
nix-channel --add https://github.com/LnL7/nix-darwin/archive/master.tar.gz darwin
nix-channel --update
```

Then run the installer:
```bash
./result/bin/darwin-installer
```

After nix-darwin install successfully, you can update the config by running:
```bash
darwin-rebuild switch
```

DON'T RUN `home-manager switch` if Home Manager is managed by nix-darwin!

https://github.com/DeterminateSystems/nix-installer?tab=readme-ov-file#determinate-nix-installer  
https://github.com/LnL7/nix-darwin  
https://nix-community.github.io/home-manager/index.xhtml#sec-install-nix-darwin-module  
